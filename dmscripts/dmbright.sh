#!/usr/bin/env bash

declare -a options=(
"40%" 
"20%" 
"10%"
)

choice=$(printf '%s\n' "${options[@]}" | dmenu -n -l 6 -p 'Change Brightess:')

if [[ "$choice" == "40%" ]]; then
    xrandr --output DP-2 --brightness 0.4

elif [ "$choice" == "20%" ]; then
    xrandr --output DP-2 --brightness 0.2

elif [ "$choice" == "10%" ]; then
    xrandr --output DP-2 --brightness 0.1

else
    echo "Exiting Script..." && exit 1
fi

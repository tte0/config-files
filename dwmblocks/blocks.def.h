//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
        {"", "awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){u1=u; t1=t;} else printf(\"Cpu: %.f%\"), ($2+$4-u1) * 100 / (t-t1)}' <(grep 'cpu ' /proc/stat) <(sleep 0.5;grep 'cpu ' /proc/stat)",   1,             0},

	{"", "free -t | awk 'NR == 2 {printf(\"Mem: %.f%\"), $3/$2*100}'",        1,             0},

	{"", "free -t | grep Swap | awk '{printf(\"Swap: %.f%\"), $3/$2*100}'",   1,             0},

	{"Updates: ", "checkupdates | wc -l",                                   1,             0},

	{"", "uptime -p",					60,		0},
	
	{"", "date '+%a %d %b %Y %H:%M:%S'",					1,	       0},

	{"", "uname -s -r",					3600,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
